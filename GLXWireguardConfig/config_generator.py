# from configparser import ConfigParser
import configparser
import ipaddress
import os
import subprocess
from io import StringIO, BytesIO

import qrcode
from mdutils.mdutils import MdUtils


class WireguardConfigGenerator(object):
    def __init__(self,
                 listen_port=None,
                 endpoint=None,
                 clients=None,
                 dns=None,
                 preshared_key=None,
                 network=None,
                 server_network_interface=None,
                 allowed_ips=None,
                 disallowed_ips=None,
                 directory=None,
                 verbose=None,
                 ):
        """

        :rtype: object
        """
        self.__listen_port = None
        self.__endpoint = None
        self.__clients = None
        self.__dns = None
        self.__preshared_key = None
        self.__ip_net_tunnel = None
        self.__iptables = None
        self.__allowed_ips = None
        self.__disallowed_ips = None
        self.__directory = None
        self.__verbose = None

        self.listen_port = listen_port
        self.endpoint = endpoint
        self.clients = clients
        self.dns = dns
        self.preshared_key = preshared_key
        self.network = network
        self.server_network_interface = server_network_interface
        self.allowed_ips = allowed_ips
        self.disallowed_ips = disallowed_ips
        self.directory = directory
        self.verbose = verbose

        self.generate_markdown = True
        self.generate_png = True

    def __enter__(self):
        self.generate_markdown = True
        self.generate_png = True
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    @property
    def listen_port(self):
        return self.__listen_port

    @listen_port.setter
    def listen_port(self, value=None):
        """
        Set the ``listen_port`` property value


        :param value:
        :return:
        """
        if value is None:
            value = 51820
        if type(value) != int:
            raise TypeError("'listen_port' property value must be a int type or None")
        if value < 1:
            raise ValueError("'listen_port' property value must be superior to 0")
        if value > 65535:
            raise ValueError("'listen_port' property value must be inferior to 65535")
        if self.listen_port != value:
            self.__listen_port = value

    @property
    def dns(self):
        """
        If you’re forwarding internet traffic through the Wireguard interface,
        then you will want a place to resolve all of your hostnames.

        The DNS option allows you to specify an alternate DNS server for your tunnel traffic.

        Return DNS Servers list like ["1.1.1.1"]

        :return: Servers list
        :rtype: list
        """
        return self.__dns

    @dns.setter
    def dns(self, value=None):
        """
        Set ``dns`` property value

        Set your DNS Server like "1.1.1.1" or empty string ""
        if not needed maybe you want to use a dns server on your server e.g. 192.168.1.1

        :param value:
        :type value: list or None
        """
        if value is None:
            value = ["1.1.1.1", "1.0.0.1"]
        if type(value) != list:
            raise TypeError("'dns' property value must be a list type or None")
        if self.dns != value:
            self.__dns = value

    @property
    def endpoint(self):
        """
        ([Peer] config section) is the remote peer's "real" IP address and port, outside of the WireGuard VPN.
        This setting tells the local host how to connect to the remote peer in order to set up a WireGuard tunnel.

        In the example config, where Endpoint = 54.91.5.139:1952 for the remote peer,
        any packets routed through the virtual WireGuard tunnel for that peer will actually be encrypted,
        wrapped in a new set of UDP packets, and sent across the Internet
        (or some other "real" network, like your corporate network) to 54.91.5.139 UDP port 1952.

        Unless you're also doing some fancy routing on the local host outside of WireGuard, if you try to
        send ping packets from the local host to this endpoint (eg ping 54.91.5.139), or if you try to access
        some other service of the remote peer from the local host via this endpoint address
        (eg navigate to http://54.91.5.139/ in a web browser), you will not be using the WireGuard tunnel --
        you will be using your regular Internet (or other "real" network) connection.

        :return: IP address and port, outside of the WireGuard VPN
        :rtype: str
        """
        return self.__endpoint

    @endpoint.setter
    def endpoint(self, value=None):
        """
        Set the ``endpoint`` property value

        :param value: The address where VPN client's try to join
        :type value: str or None
        :raise TypeError: When ``endpoint`` property value is not a str type or None
        """
        if value is None:
            value = f"example.myip.com:{self.listen_port}"

        if type(value) != str:
            raise TypeError("'endpoint' property value must be a str type or None")

        if len(value.split(":")) <= 1:
            raise ValueError("'endpoint' value must contain a IP address and a UDP port")
        try:
            int(value.split(":")[1])
        except ValueError:
            raise ValueError("'endpoint' value must contain a valid UDP port")

        if int(value.split(":")[1]) < 1 or int(value.split(":")[1]) > 65535:
            raise ValueError("'endpoint' value must contain a valid port number 16-bit integer from 1 to 65535")
        if self.endpoint != value:
            self.__endpoint = value

    @property
    def clients(self):
        """
        Number of needed clients

        :return: Clients numbers
        :rtype: int
        """
        return self.__clients

    @clients.setter
    def clients(self, value=None):
        """
        Set ``clients`` property value

        :param value: Clients numbers
        :type value: str or None
        :raise TypeError: When ``clients`` property value is not a str type or None
        """
        if value is None:
            value = 3
        if type(value) != int:
            raise TypeError("'clients' property value must be a int type or None")
        if self.clients != value:
            self.__clients = value

    @property
    def preshared_key(self):
        return self.__preshared_key

    @preshared_key.setter
    def preshared_key(self, value=None):
        if value is None:
            value = True
        if type(value) != bool:
            raise TypeError(
                "'preshared_key' property value must be a bool type or None"
            )
        if self.preshared_key != value:
            self.__preshared_key = value

    @property
    def network(self):
        """

        :return:
        :rtype: ipaddress.ip_network
        """
        return self.__ip_net_tunnel

    @network.setter
    def network(self, value=None):
        """
        Set your vpn tunnel network (example is for 10.99.99.0/24)

        :param value: Tunnel network IP
        :type value: str or None
        """
        if value is None:
            value = "10.99.99.0/24"
        if type(value) != str:
            raise TypeError("'ip_net_tunnel' property value must be a str type or None")
        # if ipaddress.ip_network(value):

        if self.network != ipaddress.ip_network(value):
            self.__ip_net_tunnel = ipaddress.ip_network(value)

    @property
    def server_network_interface(self):
        """
        server_network_interface= "eth0" (replace eth0 with the name of your network card) or
        iptables = "" if no rules needed

        :return: the name of server network card
        :rtype: str or None
        """
        return self.__iptables

    @server_network_interface.setter
    def server_network_interface(self, value=None):
        """
        If you need iptables rules then set

        :param value:  the name of server network card
        :type value: str or None
        """
        if value is None:
            if self.server_network_interface is not None:
                self.__iptables = None
            return
        if type(value) != str:
            raise TypeError("'iptables' property value must be a str type or None")
        if self.server_network_interface != value:
            self.__iptables = value

    @property
    def allowed_ips(self):
        """
        ([Peer] config section) is the set of IP addresses the local host should route to the remote peer through the
        WireGuard tunnel. This setting tells the local host what goes in tunnel.

        In the example config, where AllowedIPs = 10.129.130.1/32 for the remote peer, any packets on the local
        host destined for 10.129.130.1 will not be sent directly over your regular Internet (or other "real" network)
        connection, but instead first sent to the virtual WireGuard tunnel. WireGuard will encrypt them, wrap them
        in a new set of UDP packets, and send them across the Internet (or other "real" network) to the peer's
        endpoint, 54.91.5.139. From there, the peer will unwrap and decrypt the packets, and try to forward them
        on to 10.129.130.1.

        So if you try to send ping packets from the local host to 10.129.130.1 (eg ping 10.129.130.1),
        or try to access some other service of 10.129.130.1 (eg navigate to http://10.129.130.1 in a web browser),
        you will be using the WireGuard tunnel.

        Conversely, like you mentioned, for packets that have come through the tunnel from this remote peer,
        if they, once unwrapped and decrypted, have a source IP outside of the block(s) specified by AllowedIPs
        (eg the source IP is 10.1.1.1 instead of 10.129.130.1), the local host will drop them.

        :return: list of allowed IPS
        :rtype: list or None
        """
        return self.__allowed_ips

    @allowed_ips.setter
    def allowed_ips(self, value=None):
        if value is None:
            if self.allowed_ips is not None:
                self.__allowed_ips = None
            return
        if type(value) != list:
            raise TypeError("'allowed_ips' property value must be a list type or None")
        if self.allowed_ips != value:
            self.__allowed_ips = value

    @property
    def disallowed_ips(self):
        """
        ([Peer] config section)

        :return: list of disallowed IPS
        :rtype: list or None
        """
        return self.__disallowed_ips

    @disallowed_ips.setter
    def disallowed_ips(self, value=None):
        if value is None:
            if self.disallowed_ips is not None:
                self.__disallowed_ips = None
            return
        if type(value) != list:
            raise TypeError("'disallowed_ips' property value must be a list type or None")
        if self.disallowed_ips != value:
            self.__disallowed_ips = value

    @property
    def directory(self):
        return self.__directory

    @directory.setter
    def directory(self, value=None):
        if value is None:
            value = os.path.realpath(os.getcwd())
        if type(value) != str:
            raise TypeError("'directory' property value must be a str type or None")
        if self.directory != value:
            self.__directory = value

    @property
    def verbose(self):
        return self.__verbose

    @verbose.setter
    def verbose(self, value=None):
        if value is None:
            value = True
        if type(value) != bool:
            raise TypeError(
                "'verbose' property value must be a bool type or None"
            )
        if self.verbose != value:
            self.__verbose = value

    def get_new_keys(self):
        keys = {}
        for count in range(0, self.clients + 1):
            private = (
                subprocess.check_output("wg genkey", shell=True).decode("utf-8").strip()
            )
            public = (
                subprocess.check_output(f"echo '{private}' | wg pubkey", shell=True).decode("utf-8").strip()
            )
            shared = (
                subprocess.check_output("wg genkey", shell=True).decode("utf-8").strip()
            )
            keys[count] = {
                "PrivateKey": private,
                "PublicKey": public,
                "PresharedKey": shared,
            }
        return keys

    @staticmethod
    def get_qrcode(text=None, qrcode_type=None):
        """
        Create a QRCode

        :param qrcode_type: "png" or "ascii"
        :type qrcode_type: str
        :param text: the text from where create the qrcode
        :type text: str
        :return: return a qrcode it depend of the type (png or ascii)
        :rtype: bytes or str
        """
        if type(text) != str:
            raise TypeError("text parameter must be a str type")
        if type(qrcode_type) != str:
            raise TypeError("qrcode_type parameter must be a str type")
        if qrcode_type not in ["png", "ascii"]:
            raise ValueError("qrcode_type must be 'png' or 'txt' ")
        qr = qrcode.QRCode(
            version=1,
            error_correction=qrcode.constants.ERROR_CORRECT_L,
            box_size=4,
            border=4,
        )

        qr.add_data(text)
        qr.make(fit=True)

        if qrcode_type.lower() == "png":
            with BytesIO() as buffered:
                img = qr.make_image()
                img.save(buffered, format="PNG")
                return buffered.getvalue()
        if qrcode_type.lower() == "ascii":
            with StringIO() as buffered:
                qr.print_ascii(out=buffered, invert=True)
                return buffered.getvalue()

    def config_server(self, keys=None):
        """
        Return configparser.ConfigParser object with the server setting.

        :param keys: the key directory as return by ``generate_keys``
        :type keys: dict
        :return: configparser.ConfigParser object it contain the server setting.
        :rtype: configparser.ConfigParser
        """
        config_server = configparser.ConfigParser(strict=False, interpolation=None)
        config_server.optionxform = str
        config_server["Interface"] = {
            "Address": f"{list(self.network.hosts())[0].exploded}/{self.network.prefixlen}",
            "ListenPort": self.listen_port,
            "PrivateKey": keys[0]['PrivateKey'],
        }
        if self.server_network_interface:
            config_server["Interface"]["PostUp"] = f"iptables -A FORWARD -i %i -j ACCEPT; " \
                                                   f"iptables -t nat -A POSTROUTING -o {self.server_network_interface} -j MASQUERADE"
            config_server["Interface"]["PostDown"] = f"iptables -D FORWARD -i %i -j ACCEPT; " \
                                                     f"iptables -t nat -D POSTROUTING -o {self.server_network_interface} -j MASQUERADE"

        for client_number in range(1, self.clients + 1):
            config_server[f"Peer {client_number}"] = {
                "PublicKey": keys[client_number]["PublicKey"],
                "AllowedIPs": f"{list(self.network.hosts())[client_number].exploded}/32",
            }

            if self.preshared_key:
                config_server[f"Peer {client_number}"]["PresharedKey"] = keys[client_number]["PresharedKey"]

        return config_server

    def config_client(self, client_number=None, keys=None):
        """
        Return configparser.ConfigParser object with a client setting.

        :param client_number: The client number
        :type client_number: int
        :param keys: the key directory as return by ``generate_keys``
        :type keys: dict
        :return: configparser.ConfigParser object it contain the client setting.
        :rtype: configparser.ConfigParser
        """
        config_client = configparser.ConfigParser(strict=False, interpolation=None)
        config_client.optionxform = str
        config_client["Interface"] = {
            "Address": f"{list(self.network.hosts())[client_number].exploded}/{self.network.prefixlen}",
            "ListenPort": self.listen_port,
            "PrivateKey": keys[client_number]["PrivateKey"],
        }
        if self.dns:
            config_client["Interface"]["DNS"] = ", ".join(self.dns)

        config_client["Peer"] = {"PublicKey": keys[0]["PublicKey"]}

        if self.preshared_key:
            config_client["Peer"]["PresharedKey"] = keys[client_number]["PresharedKey"]

        if self.allowed_ips:
            config_client["Peer"]["AllowedIPs"] = f"{', '.join(self.allowed_ips)}"
        else:
            config_client["Peer"]["AllowedIPs"] = f"{list(self.network.hosts())[0].exploded}/32"

        if self.disallowed_ips:
            config_client["Peer"]["DisallowedIPs"] = f"{', '.join(self.disallowed_ips)}"

        config_client["Peer"]["Endpoint"] = self.endpoint

        return config_client

    def generate(self):
        # Generate news each call, without any storage capability ;)
        keys = self.get_new_keys()

        # Create subdirectory structure
        directory_conf = os.path.join(self.directory, "gen", "conf")
        directory_png = os.path.join(self.directory, "gen", "png")
        directory_md = os.path.join(self.directory, "gen", "md")

        for path in [directory_conf, directory_png, directory_md]:
            if not os.path.exists(path):
                os.makedirs(path)

        # Server Configuration
        config_server = StringIO()
        self.config_server(keys=keys).write(config_server)

        with open(os.path.join(directory_conf, "server.conf"), "w") as file_server:
            file_server.write(config_server.getvalue().strip())
            file_server.write("\n")

        if self.generate_markdown:
            mark_down_file = MdUtils(file_name=os.path.join(directory_md, "server.md"))
            mark_down_file.new_header(level=1, title="Server")
            mark_down_file.new_header(level=2, title='Configuration')
            mark_down_file.new_paragraph(f"``` ini\n"
                                         f"{config_server.getvalue().strip()}"
                                         f"\n```")
            mark_down_file.new_header(level=2, title='Keys')
            list_of_strings = ["Host", "PrivateKey", "PublicKey", "PresharedKey"]
            for host_number in range(self.clients + 1):
                if host_number == 0:
                    list_of_strings.extend([
                        f"Server",
                        f"{keys[host_number]['PrivateKey']}",
                        f"{keys[host_number]['PublicKey']}",
                        f"{keys[host_number]['PresharedKey']}"
                    ])
                else:
                    list_of_strings.extend([
                        f"Client {host_number}",
                        f"{keys[host_number]['PrivateKey']}",
                        f"{keys[host_number]['PublicKey']}",
                        f"{keys[host_number]['PresharedKey']}"
                    ])
            mark_down_file.new_line()
            mark_down_file.new_table(
                columns=4,
                rows=self.clients + 2,
                text=list_of_strings,
                text_align='center'
            )
            mark_down_file.create_md_file()

        if self.verbose:
            text = " Server-Conf "
            print("*" * int(40 - len(text) / 2) + f"{text}" + "*" * int(40 - len(text) / 2))
            print(config_server.getvalue().strip())
            print("")

        # Clients Configuration
        for client_number in range(1, self.clients + 1):
            config_client = StringIO()
            self.config_client(client_number=client_number, keys=keys).write(config_client)

            with open(os.path.join(directory_conf, f"client_{client_number}.conf"), "w") as file_client:
                file_client.write(config_client.getvalue().strip())
                file_client.write("\n")

            if self.generate_png:
                with open(os.path.join(directory_png, f"client_{client_number}.png"), "wb") as file_png:
                    file_png.write(self.get_qrcode(text=config_client.getvalue().strip(), qrcode_type="png"))

            if self.generate_markdown:
                mark_down_file = MdUtils(
                    file_name=os.path.join(directory_md, f"client_{client_number}.md")
                )
                mark_down_file.new_header(level=1, title=f"Client {client_number}")
                mark_down_file.new_header(level=2, title='Configuration')
                mark_down_file.new_paragraph(f"``` ini\n"
                                             f"{config_client.getvalue().strip()}"
                                             f"\n```")
                mark_down_file.new_header(level=2, title='QR Code')
                mark_down_file.write('``` text\n')
                for line in self.get_qrcode(text=config_client.getvalue().strip(), qrcode_type="ascii").splitlines():
                    mark_down_file.write(f"{line}\n")
                mark_down_file.write('```\n')
                mark_down_file.create_md_file()

            if self.verbose:
                text = f" Client-Conf {client_number} "
                print("*" * int(40 - len(text) / 2) + f"{text}" + "*" * int(40 - len(text) / 2))
                print(self.get_qrcode(text=config_client.getvalue().strip(), qrcode_type="ascii"))
                print(config_client.getvalue().strip())
                print("")

        if self.verbose:
            print("")

            for host_number, keys in keys.items():
                if host_number == 0:
                    print(f"Server {host_number}: {keys}")
                else:
                    print(f"Client {host_number}: {keys}")
