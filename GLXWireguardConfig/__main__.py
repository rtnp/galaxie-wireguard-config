import argparse
from GLXWireguardConfig.config_generator import WireguardConfigGenerator


def main(argv=None):

    wg_config_parser = argparse.ArgumentParser(description="Commands as arguments")
    wg_config_parser.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        default=False,
        help="Display information",
    )
    wg_config_parser.add_argument(
        "-P",
        "--preshared-keys",
        action="store_true",
        default=False,
        help="Preshared key will be use",
    )
    wg_config_parser.add_argument(
        "-p",
        "--port",
        type=int,
        help="Port use by the VPN",
    )
    wg_config_parser.add_argument(
        "-c",
        "--clients",
        type=int,
        help="Number of clients in the VPN",
    )
    wg_config_parser.add_argument(
        "-e",
        "--end-point",
        type=str,
        help="([Peer] config section) is the remote peer's 'real' IP address and port, outside of the "
             "WireGuard VPN. This setting tells the local host how to connect to the remote peer in order "
             "to set up a WireGuard tunnel.",
    )
    wg_config_parser.add_argument(
        '-d',
        '--dns',
        action='append',
        help='Set DNS servers',
    )
    wg_config_parser.add_argument(
        '-n',
        '--network',
        type=str,
        help='Network use by the VPN',
    )
    wg_config_parser.add_argument(
        '-D',
        '--disallowed-ips',
        action='append',
        help='([Peer] config section)',
    )
    wg_config_parser.add_argument(
        '-A',
        '--allowed-ips',
        action='append',
        help='([Peer] config section) is the set of IP addresses the local host should route to the remote peer '
             'through the WireGuard tunnel. This setting tells the local host what goes in tunnel.',
    )
    wg_config_parser.add_argument(
        '-i',
        '--server-network-interface',
        type=str,
        help='Network Interface of the VPN server, by example: eth0',
    )
    args = wg_config_parser.parse_args(argv)
    with WireguardConfigGenerator() as wireguard_config_generator:
        wireguard_config_generator.verbose = args.verbose
        wireguard_config_generator.listen_port = args.port
        wireguard_config_generator.endpoint = args.end_point
        wireguard_config_generator.clients = args.clients
        wireguard_config_generator.dns = args.dns
        wireguard_config_generator.preshared_key = args.preshared_keys
        wireguard_config_generator.network = args.network
        wireguard_config_generator.allowed_ips = args.allowed_ips
        wireguard_config_generator.disallowed_ips = args.disallowed_ips
        wireguard_config_generator.server_network_interface = args.server_network_interface
        wireguard_config_generator.generate()


if __name__ == "__main__":
    main()
