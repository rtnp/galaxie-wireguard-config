#!/usr/bin/env python
# -*- coding: utf-8 -*-
# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html

APPLICATION_VERSION = "0.1"
APPLICATION_AUTHORS = ["Tuxa", "Mo"]
APPLICATION_NAME = "Galaxie Wireguard Config"
APPLICATION_COPYRIGHT = "2016-2021 - Galaxie Wireguard Config Team all right reserved"