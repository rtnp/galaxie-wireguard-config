import os
import unittest

from GLXWireguardConfig.config_generator import WireguardConfigGenerator


class TestWireguardConfigGenerator(unittest.TestCase):
    def setUp(self) -> None:
        with WireguardConfigGenerator() as wireguard_config_generator:
            self.wireguard_config_generator = wireguard_config_generator

    def test_dns(self):
        self.wireguard_config_generator.dns = ["2.2.2.2", "3.3.3.3"]
        self.assertEqual(["2.2.2.2", "3.3.3.3"], self.wireguard_config_generator.dns)

        self.wireguard_config_generator.dns = None
        self.assertEqual(["1.1.1.1", "1.0.0.1"], self.wireguard_config_generator.dns)

        self.assertRaises(
            TypeError, setattr, self.wireguard_config_generator, "dns", 42
        )

    def test_listen_port(self):
        self.wireguard_config_generator.listen_port = 42
        self.assertEqual(42, self.wireguard_config_generator.listen_port)

        self.wireguard_config_generator.listen_port = None
        self.assertEqual(51820, self.wireguard_config_generator.listen_port)

        self.assertRaises(
            TypeError,
            setattr,
            self.wireguard_config_generator,
            "listen_port",
            "Hello.42",
        )
        self.assertRaises(
            ValueError, setattr, self.wireguard_config_generator, "listen_port", -1
        )
        self.assertRaises(
            ValueError, setattr, self.wireguard_config_generator, "listen_port", 65536
        )

    def test_endpoint(self):
        self.wireguard_config_generator.endpoint = "Hello:42"
        self.assertEqual("Hello:42", self.wireguard_config_generator.endpoint)

        self.wireguard_config_generator.endpoint = None
        self.assertEqual(f"example.myip.com:{self.wireguard_config_generator.listen_port}",
                         self.wireguard_config_generator.endpoint)

        self.assertRaises(
            TypeError, setattr, self.wireguard_config_generator, "endpoint", 42
        )

        self.assertRaises(
            ValueError, setattr, self.wireguard_config_generator, "endpoint", "Hello.42"
        )
        self.assertRaises(
            ValueError, setattr, self.wireguard_config_generator, "endpoint", "42:Hello"
        )
        self.assertRaises(
            ValueError, setattr, self.wireguard_config_generator, "endpoint", "42:420000"
        )
        self.assertRaises(
            ValueError, setattr, self.wireguard_config_generator, "endpoint", "42.:-1"
        )

    def test_clients(self):
        self.wireguard_config_generator.clients = 42
        self.assertEqual(42, self.wireguard_config_generator.clients)

        self.wireguard_config_generator.clients = None
        self.assertEqual(3, self.wireguard_config_generator.clients)

        self.assertRaises(
            TypeError, setattr, self.wireguard_config_generator, "clients", "Hello.42"
        )

    def test_preshared_key(self):
        self.wireguard_config_generator.preshared_key = None
        self.assertTrue(self.wireguard_config_generator.preshared_key)

        self.wireguard_config_generator.preshared_key = False
        self.assertFalse(self.wireguard_config_generator.preshared_key)

        self.wireguard_config_generator.preshared_key = None
        self.assertTrue(self.wireguard_config_generator.preshared_key)

        self.assertRaises(
            TypeError,
            setattr,
            self.wireguard_config_generator,
            "preshared_key",
            "Hello.42",
        )

    def test_network(self):
        self.wireguard_config_generator.network = "192.168.0.0/16"
        self.assertEqual(
            "192.168.0.0/16", self.wireguard_config_generator.network.exploded
        )

        self.wireguard_config_generator.network = None
        self.assertEqual(
            "10.99.99.0/24", self.wireguard_config_generator.network.exploded
        )

        self.assertRaises(
            TypeError, setattr, self.wireguard_config_generator, "network", 42
        )
        self.assertRaises(
            ValueError,
            setattr,
            self.wireguard_config_generator,
            "network",
            "Hello.42",
        )

    def test_iptables(self):
        self.wireguard_config_generator.server_network_interface = "eth0"
        self.assertEqual("eth0", self.wireguard_config_generator.server_network_interface)

        self.wireguard_config_generator.server_network_interface = None
        self.assertIsNone(self.wireguard_config_generator.server_network_interface)

        self.assertRaises(
            TypeError, setattr, self.wireguard_config_generator, "server_network_interface", 42
        )

    def test_allowed_ips(self):
        self.wireguard_config_generator.allowed_ips = ["192.168.1.2/32"]
        self.assertEqual(
            ["192.168.1.2/32"], self.wireguard_config_generator.allowed_ips
        )

        self.wireguard_config_generator.allowed_ips = None
        self.assertIsNone(self.wireguard_config_generator.allowed_ips)

        self.assertRaises(
            TypeError, setattr, self.wireguard_config_generator, "allowed_ips", 42
        )

    def test_disallowed_ips(self):
        self.wireguard_config_generator.disallowed_ips = ["192.168.1.2/32"]
        self.assertEqual(
            ["192.168.1.2/32"], self.wireguard_config_generator.disallowed_ips
        )

        self.wireguard_config_generator.disallowed_ips = None
        self.assertIsNone(self.wireguard_config_generator.disallowed_ips)

        self.assertRaises(
            TypeError, setattr, self.wireguard_config_generator, "disallowed_ips", 42
        )

    def test_directory(self):
        self.wireguard_config_generator.directory = os.path.dirname(
            os.path.abspath(__file__)
        )
        self.assertEqual(
            os.path.dirname(os.path.abspath(__file__)),
            self.wireguard_config_generator.directory,
        )

        self.wireguard_config_generator.directory = None
        self.assertEqual(
            os.path.realpath(os.getcwd()), self.wireguard_config_generator.directory
        )

        self.assertRaises(
            TypeError, setattr, self.wireguard_config_generator, "directory", 42
        )

    def test_verbose(self):
        self.wireguard_config_generator.verbose = None
        self.assertTrue(self.wireguard_config_generator.verbose)

        self.wireguard_config_generator.verbose = False
        self.assertFalse(self.wireguard_config_generator.verbose)

        self.wireguard_config_generator.verbose = None
        self.assertTrue(self.wireguard_config_generator.verbose)

        self.assertRaises(
            TypeError,
            setattr,
            self.wireguard_config_generator,
            "verbose",
            "Hello.42",
        )

    def test_keys(self):
        self.wireguard_config_generator.clients = 5
        keys = self.wireguard_config_generator.get_new_keys()

        for item in keys:
            self.assertTrue(item in [0, 1, 2, 3, 4, 5])
            for key_name in keys[item]:
                self.assertTrue(key_name in ["PresharedKey", "PrivateKey", "PublicKey"])

    def test_generate_config_server(self):
        self.wireguard_config_generator.clients = 2
        self.wireguard_config_generator.preshared_key = True
        self.wireguard_config_generator.server_network_interface = "eth0"

        keys = self.wireguard_config_generator.get_new_keys()

        self.assertEqual(
            "10.99.99.1/24",
            self.wireguard_config_generator.config_server(keys=keys).get("Interface", "Address")
        )
        self.assertEqual(
            "51820",
            self.wireguard_config_generator.config_server(keys=keys).get("Interface", "ListenPort")
        )
        self.assertEqual(
            keys[0]['PrivateKey'],
            self.wireguard_config_generator.config_server(keys=keys).get("Interface", "PrivateKey")
        )
        # Client 1
        self.assertEqual(
            keys[1]['PublicKey'],
            self.wireguard_config_generator.config_server(keys=keys).get("Peer 1", "PublicKey")
        )
        self.assertEqual(
            f"{list(self.wireguard_config_generator.network.hosts())[1].exploded}/32",
            self.wireguard_config_generator.config_server(keys=keys).get("Peer 1", "AllowedIPs")
        )
        self.assertEqual(
            keys[1]["PresharedKey"],
            self.wireguard_config_generator.config_server(keys=keys).get("Peer 1", "PresharedKey")
        )
        # Client 2
        self.assertEqual(
            keys[2]['PublicKey'],
            self.wireguard_config_generator.config_server(keys=keys).get("Peer 2", "PublicKey")
        )
        self.assertEqual(
            f"{list(self.wireguard_config_generator.network.hosts())[2].exploded}/32",
            self.wireguard_config_generator.config_server(keys=keys).get("Peer 2", "AllowedIPs")
        )
        self.assertEqual(
            keys[2]["PresharedKey"],
            self.wireguard_config_generator.config_server(keys=keys).get("Peer 2", "PresharedKey")
        )

    def test_generate_config_client(self):

        self.wireguard_config_generator.listen_port = 42
        self.wireguard_config_generator.endpoint = "vpn:42"
        self.wireguard_config_generator.clients = 2
        self.wireguard_config_generator.dns = ["1.1.1.1"]
        self.wireguard_config_generator.preshared_key = True
        self.wireguard_config_generator.network = "192.99.99.0/24"
        self.wireguard_config_generator.allowed_ips = ['0.0.0.0']
        self.wireguard_config_generator.disallowed_ips = ['192.99.98.0/24']

        keys = self.wireguard_config_generator.get_new_keys()

        self.assertEqual(
            "192.99.99.2/24",
            self.wireguard_config_generator.config_client(client_number=1, keys=keys).get("Interface", "Address")
        )
        self.assertEqual(
            str(self.wireguard_config_generator.listen_port),
            self.wireguard_config_generator.config_client(client_number=1, keys=keys).get("Interface", "ListenPort")
        )
        self.assertEqual(
            keys[1]['PrivateKey'],
            self.wireguard_config_generator.config_client(client_number=1, keys=keys).get("Interface", "PrivateKey")
        )
        self.assertEqual(
            ", ".join(self.wireguard_config_generator.dns),
            self.wireguard_config_generator.config_client(client_number=1, keys=keys).get("Interface", "DNS")
        )

        self.assertEqual(
            ", ".join(self.wireguard_config_generator.allowed_ips),
            self.wireguard_config_generator.config_client(client_number=1, keys=keys).get("Peer", "AllowedIPs")
        )

        self.assertEqual(
            ", ".join(self.wireguard_config_generator.disallowed_ips),
            self.wireguard_config_generator.config_client(client_number=1, keys=keys).get("Peer", "DisallowedIPs")
        )

        self.wireguard_config_generator.allowed_ips = None

        self.assertEqual(
            f"{list(self.wireguard_config_generator.network.hosts())[0].exploded}/32",
            self.wireguard_config_generator.config_client(client_number=1, keys=keys).get("Peer", "AllowedIPs")
        )

    def test_generate(self):
        import tempfile
        with tempfile.TemporaryDirectory() as temporary_directory:
            self.wireguard_config_generator.directory = temporary_directory
            self.wireguard_config_generator.listen_port = 42
            self.wireguard_config_generator.endpoint = "vpn:42"
            self.wireguard_config_generator.clients = 2
            self.wireguard_config_generator.dns = ["1.1.1.1"]
            self.wireguard_config_generator.preshared_key = True
            self.wireguard_config_generator.network = "192.99.99.0/24"
            self.wireguard_config_generator.allowed_ips = ['0.0.0.0']
            self.wireguard_config_generator.disallowed_ips = ['192.99.98.0/24']
            self.wireguard_config_generator.verbose = True
            self.wireguard_config_generator.generate_png = True
            self.wireguard_config_generator.generate_markdown = True
            self.wireguard_config_generator.generate()

    def test_grcode(self):
        self.assertEqual(bytes, type(self.wireguard_config_generator.get_qrcode(text="Hello.42", qrcode_type="png")))
        self.assertEqual(str, type(self.wireguard_config_generator.get_qrcode(text="Hello.42", qrcode_type="ascii")))

        data = """█████████████████████████████
█████████████████████████████
████ ▄▄▄▄▄ ███▄█ █ ▄▄▄▄▄ ████
████ █   █ █▄█▄█▀█ █   █ ████
████ █▄▄▄█ ██ ▀ ▄█ █▄▄▄█ ████
████▄▄▄▄▄▄▄█ █ █▄█▄▄▄▄▄▄▄████
████▄ ▄▀▄▄▄▄ ▀ ▀▄ ▄█▀█▄▀▀████
████ █▄ ▄█▄  ▄▄ ▀▀█▀▄▄▄ ▄████
█████▄████▄█ █▀▄██ ▄ █▀▀█████
████ ▄▄▄▄▄ █ █ █▄█▀▀ █▄ ▄████
████ █   █ █▄▀▄▀▄▀▄ ▀█▀▀█████
████ █▄▄▄█ █  ▀ ▀ ▄█ █▄▀▄████
████▄▄▄▄▄▄▄█▄▄▄▄█▄▄▄███▄▄████
█████████████████████████████
▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀
"""
        self.assertEqual(data, self.wireguard_config_generator.get_qrcode(text="Hello.42", qrcode_type="ascii"))

        self.assertRaises(TypeError, self.wireguard_config_generator.get_qrcode, text=42, qrcode_type="ascii")
        self.assertRaises(TypeError, self.wireguard_config_generator.get_qrcode, text="Hello.42", qrcode_type=42)
        self.assertRaises(ValueError, self.wireguard_config_generator.get_qrcode, text="Hello.42", qrcode_type="42")


if __name__ == "__main__":
    unittest.main()
