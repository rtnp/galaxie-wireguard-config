.PHONY: help packages-install header venv-create prepare tests docs clean
#SHELL=/bin/bash

#!make
include .env
export $(shell sed 's/=.*//' .env)

help:
	@echo  'CleaningUp:'
	@echo  '  clean               - Remove every created directory and restart from scratch'
	@echo  ''
	@echo  'Documentations:'
	@echo  '  docs                - Install Sphinx and requirements, then build documentations'
	@echo  ''
	@echo  'VirtualEnv:'
	@echo  '  venv-create         - Create virtual env directory with python venv module'
	@echo  ''
	@echo  'Preparation:'
	@echo  '   packages-install   - Easy way to install Python and wireguard'
	@echo  '   prepare            - Easy way for make everything'
	@echo  ''
	@echo  'Tests:'
	@echo  '   tests              - Easy way for make tests over a CI'

packages-install:
	@echo 'INSTALL PYTHON'
	apt-get update && apt install -y python3 python3-pip python3-venv wireguard-tools

header:
	@echo "*********************** GALAXIE WIREGUARD CONFIG MAKEFILE **********************"
	@echo ""
	@echo "GNU GENERAL PUBLIC LICENSE V3 OR LATER (GPLV3+)"
	@echo "LOADER `uname -v`"
	@echo "EXEC MAKE"
	@echo "********************************************************************************"

venv-create: header
	@ if test -d $(DEV_VENV_DIRECTORY);\
	then echo "[  OK  ] VIRTUAL ENVIRONMENT";\
	else echo "[CREATE] VIRTUAL ENVIRONMENT" &&\
		cd $(DEV_DIRECTORY) && python3 -m venv $(DEV_VENV_NAME);\
	fi

prepare: venv-create
	@${DEV_VENV_ACTIVATE} && pip3 install -U pip --no-cache-dir --quiet &&\
	echo "[  OK  ] PIP3" || \
	echo "[FAILED] PIP3"

	@${DEV_VENV_ACTIVATE} && pip3 install -U wheel --no-cache-dir --quiet &&\
	echo "[  OK  ] WHEEL" || \
	echo "[FAILED] WHEEL"

	@${DEV_VENV_ACTIVATE} && pip3 install -U setuptools --no-cache-dir --quiet &&\
	echo "[  OK  ] SETUPTOOLS" || \
	echo "[FAILED] SETUPTOOLS"

	@${DEV_VENV_ACTIVATE} && pip3 install -U green --no-cache-dir --quiet &&\
	echo "[  OK  ] GREEN" || \
	echo "[FAILED] GREEN"

	@${DEV_VENV_ACTIVATE} && pip install -U --no-cache-dir -q Pygments && pip install --quiet -e . &&\
	echo "[  OK  ] REQUIREMENTS" || \
	echo "[FAILED] REQUIREMENTS"

tests: prepare
	@echo '********************************** RUN TESTS ***********************************'
	@ ${DEV_VENV_ACTIVATE} && python setup.py tests

clean: header
	@echo '********************************* CLEANING UP **********************************'
	rm -rf ./venv
	rm -rf ~/.cache/pip
	rm -rf ./.eggs
	rm -rf ./*.egg-info
	rm -rf .coverage

docs: prepare
	@echo '***************************** BUILD DOCUMENTATIONS *****************************'
	@ ${DEV_VENV_ACTIVATE} &&\
	pip3 install --upgrade pip setuptools wheel --no-cache-dir --quiet &&\
  pip3 install -r docs/requirements.txt --no-cache-dir --quiet &&\
	cd $(DEV_DIRECTORY)/docs &&\
	sphinx-apidoc -e -f -o source/ ../GLXWireguardConfig/ &&\
	make html
