------------------------
GALAXIE WIREGUARD CONFIG
------------------------
CHANGELOGS
----------
**0.1 - 20210920**
  * Use Makefile
  * Use SetupTools
  * Use ConfigParser library as data source
  * Use ipaddress library to manipulate true IP's
  * Use ARGParser
  * Provide a tools call wg-config
  * Reach IT transformation
