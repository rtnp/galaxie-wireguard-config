#!/bin/sh

wg-config \
--network 192.168.42.0/24 \
--port 51820 \
--server-network-interface eth0 \
--clients 5 \
--dns 1.1.1.1 \
--dns 1.0.0.1 \
--allowed-ips 0.0.0.0/0 \
--verbose && \
sed -i 's/Peer [0-9]*/Peer/g' gen/conf/server.conf
