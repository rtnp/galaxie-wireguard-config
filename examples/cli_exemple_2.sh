#!/bin/sh

wg-config \
--network 192.168.42.0/24 \
--port 51820 \
--server-network-interface eth0 \
--clients 42 \
--dns 192.168.42.1 \
--dns 1.1.1.1 \
--allowed-ips 192.168.42.0/24 \
--allowed-ips 192.168.43.0/24 \
--disallowed-ips 192.168.44.0/24 \
--disallowed-ips 192.168.45.0/24 && \
sed -i 's/Peer [0-9]*/Peer/g' gen/conf/server.conf

